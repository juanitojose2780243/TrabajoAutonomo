package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);



        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        //EN ESTE CODIGO SE RECIVE LOS DATOS DEL OBJECTID
        String object_id = getIntent().getStringExtra("object_id");

        //EN ESTE CODIGO SE DEFINE EL EL CAMPO DESCRIPCION
        //ADEMAS SE ESTABLECE LA FUNCION DE EL SCROLL PARA NAVEGAR EN EL CAMPO DESCRIPCION
        TextView description = (TextView) findViewById(R.id.textViewDescripcion);
        description.setMovementMethod(LinkMovementMethod.getInstance());


        // INICIO - CODE6


        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>(){
            @Override
            public void done(DataObject object, DataException e){
                if (e == null) {

                    //////3.3
                    //POR MEDIO DE ESTE CODIGO SE ACCEDE A LAS PROPIEDADES DEL OBJECT
                    //SE ESTABLECEN DIRECTAMENTE POR MEDIO DEL FINDVIEWBYID

                    TextView title = (TextView) findViewById(R.id.textViewNombre);
                    TextView price = (TextView) findViewById(R.id.textViewPrecio);
                    ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);
                    TextView description = (TextView) findViewById(R.id.textViewDescripcion);

                    ///////3.4
                    //POR MEDIO DE ESTE CODIGO SE RELLENAN LAS PROPIEDADES DEL PUNTO ANTERIOR POR MEDIO DEL SET
                    // ADEMAS SE ESTABLECE EL CODIGO UNICODE PARA EL SIMBOLO DE $ PARA EL PRECIO

                    title.setText((String) object.get("name"));
                    price.setText((String) object.get("price") + "\u0024");
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));
                    description.setText((String) object.get("description"));
                }else{
                    ////error




                }
            }

        });


        // FIN - CODE6

    }

}
